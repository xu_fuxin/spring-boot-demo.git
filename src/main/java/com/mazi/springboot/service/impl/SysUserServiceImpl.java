package com.mazi.springboot.service.impl;

import com.mazi.springboot.entity.SysUser;
import com.mazi.springboot.mapper.SysUserMapper;
import com.mazi.springboot.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mazi
 * @since 2022-07-04
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
