package com.mazi.springboot.service;

import com.mazi.springboot.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mazi
 * @since 2022-07-04
 */
public interface SysUserService extends IService<SysUser> {

}
