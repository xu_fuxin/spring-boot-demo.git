package com.mazi.springboot.mapper;

import com.mazi.springboot.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mazi
 * @since 2022-07-04
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
