package com.mazi.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @className: SpringbootApp
 * @description: TODO 类描述
 * @author: xufuxin
 * @date: 2022/7/3
 **/
@SpringBootApplication
@MapperScan("com.mazi.springboot.mapper")
public class SpringbootApp {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootApp.class);
    }
}