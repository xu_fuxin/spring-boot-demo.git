package com.mazi.springboot.controller;


import com.mazi.springboot.common.Result;
import com.mazi.springboot.entity.SysUser;
import com.mazi.springboot.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author mazi
 * @since 2022-07-04
 */
@RestController
@RequestMapping("/sys-user")
public class SysUserController {

    @Autowired
    SysUserService sysUserService;

    @GetMapping("findAll")
    public Result findAll() {
        List<SysUser> list = sysUserService.list();
        return Result.succ(list);
    }

    @PostMapping("insertUser")
    public Result insertUser(SysUser sysUser) {
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        sysUser.setId(uuid);
        sysUser.setCreateTime(LocalDateTime.now());
        sysUserService.save(sysUser);
        return Result.succ(null);
    }


}
